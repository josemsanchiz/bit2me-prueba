# bit2me-prueba - José Miguel Sanchiz

## Instrucciones para el funcionamiento

Se han creado dos scripts en bash para facilitar el lanzamiento del servidor en desarrollo y en la maquina
Para lanzar en desarrollo solo tenemos que ejecutar ./start-dev.sh y nos arrancará el servidor de Node, hay que tener en cuenta
que, de esta forma, no arranca ninguna instancia de Redis ni de MongoDB, tenemos que tenerlas corriendo en local

Para lanzar en la maquina destino solo tenemos que ejecutar ./start.sh. Este script lanza el docker-compose en detach mode
y se encarga de levantar las instancias de MongoDB y Redis. A tener en cuenta, tenemos que tener Docker instalado.

