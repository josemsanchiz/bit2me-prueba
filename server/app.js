const cookieParser = require('cookie-parser');
const express = require('express');
const httpErrors = require('http-errors');
const logger = require('morgan');
const path = require('path');
const cronService = require('./services/cron');

const indexRouter = require('./routes/index');

const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Load and execute cron service;
cronService.cron();

// USE CORS
app.use(cors());

// Load mongoDB Connection
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.connect(
  'mongodb://mongodb:27017',
  { 
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
);

mongoose.connection.on('error', (err) => {
  // console.log('MongoDB connection error: ', err);
  throw err;
});
mongoose.connection.on('connected', () => {
  console.log('MongoDB database connected!');
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(httpErrors(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
