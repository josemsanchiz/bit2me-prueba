const mongoose = require('mongoose');

const PricesSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now
  },
  prices: {
    btc: {
      price: Number,
      change_1h: Number
    },
    eth: {
      price: Number,
      change_1h: Number
    },
    ltc: {
      price: Number,
      change_1h: Number
    },
    xrp: {
      price: Number,
      change_1h: Number
    }
  }
})

module.exports = mongoose.model('Prices', PricesSchema);