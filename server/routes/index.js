const { Router } = require('express');

const router = Router();

const loggerService = require('../services/logger');
const CoinService = require('../services/coinmarketcap');

/* GET index page. */
router.get('/', (req, res) => {
  res.json({
    title: 'Express'
  });
});

router.get('/prices', (req, res) => {
  CoinService.coinmarketcap.getPrices()
    .then((data) => {
      loggerService.logger.log({
        level: 'info',
        message: Date.now() + ':GET/prices called'
      })
      res.status(200)
        .json({
          code: 200,
          data,
          message: 'success'
        })
    })
    .catch((error) => {
      loggerService.logger.log({
        level: 'error',
        message: Date.now() + ':GET/prices error'
      })
      res.status(500)
        .json({
          code: 500,
          data: error,
          message: 'error'
        })
    })
})

module.exports = router;
