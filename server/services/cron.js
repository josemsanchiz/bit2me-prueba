const CronJob = require('cron').CronJob;
const Cache = require('./redis');
const CoinService = require('./coinmarketcap');
const loggerService = require('./logger');
const mailerService = require('./mailer');

const io = require('socket.io')(4900, {
  path: '/',
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

io.on('connection', (socket) => {
  console.log('New connection')
  loggerService.logger.log({
    level: 'info',
    message: Date.now() + ':SOCKET: New Connection ' + socket.id
  })
})


const connect = () => {
  // First load a price and set in redis!
  CoinService.coinmarketcap.getPrices()
    .then((data) => {
      // Prices update
      new CronJob('* * * * * ', () => {
        CoinService.coinmarketcap.setPrices()
          .then((newPrice) => {
            loggerService.logger.log({
              level: 'info',
              message: Date.now() + ':CRON: Prices updated '
            })
            io.sockets.emit('prices:changed');
          })
      }, null, true, 'Europe/Madrid')

      new CronJob('0 * * * * ', () => {
        Cache.redis.get('coinmarket:prices', (err, data) => {
          loggerService.logger.log({
            level: 'info',
            message: Date.now() + ':CRON: Mail sended'
          })
          mailerService.mailjet(JSON.stringify(data.prices))
        })
      }, null, true, 'Europe/Madrid')
    })
  
}

module.exports.cron = connect;