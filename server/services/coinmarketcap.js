const CoinmarketCap = require('coinmarketcap-api');

const apiKey = '55f66a76-8f81-43f2-8b36-4c9465f9ff69';

const client = new CoinmarketCap(apiKey);

const Cache = require('../services/redis');

const Prices = require('../models/prices');

const getPrices = () => {
  return new Promise((resolve, reject) => {
    let prices = []
    // First obtain last price from redis;
    Cache.redis.get('coinmarket:prices', (err, data) => {
      // If there are prices push to the prices array and continue
      if (data !== null) {
        prices.push(JSON.parse(data));
        Prices.find()
          .limit(100)
          .sort({
            timestamp: -1
          })
          .then((bbddprices) => {
            prices = [...prices, ...bbddprices];
            resolve(prices);
          })
      } else {
        client.getQuotes({
            symbol: ['BTC', 'ETH', 'LTC', 'XRP']
          })
          .then((apidata) => {
            // console.log('sd', data.data);
            let price = {
              timestamp: Date.now(),
              prices: {
                btc: {
                  price: apidata.data.BTC.quote.USD.price,
                  change_1h: apidata.data.BTC.quote.USD.percent_change_1h
                },
                eth: {
                  price: apidata.data.ETH.quote.USD.price,
                  change_1h: apidata.data.ETH.quote.USD.percent_change_1h
                },
                ltc: {
                  price: apidata.data.LTC.quote.USD.price,
                  change_1h: apidata.data.LTC.quote.USD.percent_change_1h
                },
                xrp: {
                  price: apidata.data.XRP.quote.USD.price,
                  change_1h: apidata.data.XRP.quote.USD.percent_change_1h
                }
              }
            }
            Cache.redis.set('coinmarket:prices', JSON.stringify(price));
            resolve(price);
          })
          .catch((error) => {
            reject(error);
          })
      }
    })


  })
}

const setPrices = () => {
  return new Promise((resolve, reject) => {
    let prices = []
    // Persist price in mongo
    Cache.redis.get('coinmarket:prices', (err, data) => {
      Prices.create({
        prices: JSON.parse(data).prices
      })
        .then((newPrice) => {
          // Load new price in redis
          client.getQuotes({
              symbol: ['BTC', 'ETH', 'LTC', 'XRP']
            })
            .then((apidata) => {
              // console.log('sd', data.data);
              let price = {
                timestamp: Date.now(),
                prices: {
                  btc: {
                    price: apidata.data.BTC.quote.USD.price,
                    change_1h: apidata.data.BTC.quote.USD.percent_change_1h
                  },
                  eth: {
                    price: apidata.data.ETH.quote.USD.price,
                    change_1h: apidata.data.ETH.quote.USD.percent_change_1h
                  },
                  ltc: {
                    price: apidata.data.LTC.quote.USD.price,
                    change_1h: apidata.data.LTC.quote.USD.percent_change_1h
                  },
                  xrp: {
                    price: apidata.data.XRP.quote.USD.price,
                    change_1h: apidata.data.XRP.quote.USD.percent_change_1h
                  }
                }
              }
              Cache.redis.set('coinmarket:prices', JSON.stringify(price));
              resolve(price);
            })
            .catch((error) => {
              reject(error);
            })
        })
        .catch((error) => {
          reject(error);
        })
    })
  })
}

module.exports.coinmarketcap = {
  getPrices,
  setPrices
}