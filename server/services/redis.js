const redis = require('redis');

const client = redis.createClient({host: 'redis'});

client.on('error', (err) => {
  console.log('Redis Error: ', err);
})

module.exports.redis = client;