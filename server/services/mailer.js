const Mailjet = require('node-mailjet').connect('b607dbc5182647866d1b917996edf5db', 'e46426e24d3f5c61ad67d9088fed6483');

const sendMail = (prices) => {
  const requestEnglish = Mailjet
    .post("send", {
      'version': 'v3.1'
    })
    .request({
      "Messages": [{
        "From": {
          "Email": "josem.sanchiz@gmail.com",
          "Name": "Jose Miguel"
        },
        "To": [{
          "Email": "josem.sanchiz@gmail.com",
          "Name": "Jose Miguel"
        },
        {
          "Email": "dev-test@team.bit2me.com",
          "Name": "Bit2Me dev test"
        }
        ],
        "Subject": "Bit2me test updated prices",
        "TextPart": "Hi! This is the latest cripto prices: " + prices,

      }]
    })
    requestEnglish
      .then((result) => {
        const requestSpanish = Mailjet
          .post("send", {
            'version': 'v3.1'
          })
          .request({
            "Messages": [{
              "From": {
                "Email": "josem.sanchiz@gmail.com",
                "Name": "Jose Miguel"
              },
              "To": [{
                "Email": "josem.sanchiz@gmail.com",
                "Name": "Jose Miguel"
              }],
              "Subject": "Bit2me actualización de precios",
              "TextPart": "Hola! Esta es la última actualización de precios: " + prices,

            }]
          })

          requestSpanish
            .then((other) => {
              console.log('Sended!')
            })
            .catch((err) => {

            })
      })
      .catch((err) => {

      })
}

module.exports.mailjet = sendMail;