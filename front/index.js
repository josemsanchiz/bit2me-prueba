const socket = io('http://localhost:4900');

socket.on('prices:changed', (msg) => {
  $('#lastPrice').empty();
  $('#historical').empty();
  $('.toast').toast({
    autohide: true,
    delay: 1000
  })
  loadPrices();
})

function loadPrices() {
  $.ajax({
      url: 'http://localhost:3000/prices'
    })
    .done((data) => {
      $('#lastPrice').append(`
    <tr>
      <td>${data.data[0].prices.btc.price}</td>
      <td>${data.data[0].prices.btc.change_1h} %</td>
      <td>${data.data[0].prices.eth.price}</td>
      <td>${data.data[0].prices.eth.change_1h} %</td>
      <td>${data.data[0].prices.ltc.price}</td>
      <td>${data.data[0].prices.ltc.change_1h} %</td>
      <td>${data.data[0].prices.xrp.price}</td>
      <td>${data.data[0].prices.xrp.change_1h} %</td>
    </tr>
  `)
  for (let i = 1; i < data.data.length; i++) {
    $('#historical').append(`
      <tr>
        <td>${i}</td>
        <td>${data.data[i].timestamp}</td>
        <td>${data.data[i].prices.btc.price}</td>
        <td>${data.data[i].prices.btc.change_1h} %</td>
        <td>${data.data[i].prices.eth.price}</td>
        <td>${data.data[i].prices.eth.change_1h} %</td>
        <td>${data.data[i].prices.ltc.price}</td>
        <td>${data.data[i].prices.ltc.change_1h} %</td>
        <td>${data.data[i].prices.xrp.price}</td>
        <td>${data.data[i].prices.xrp.change_1h} %</td>
      </tr>
    `)
  }
    })
  
}

$(document).ready(() => {
  loadPrices();
})